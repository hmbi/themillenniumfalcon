# For generating the PDF reports
from PIL import Image
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.utils import ImageReader
import reportlab.lib, reportlab.platypus
## Create a classes that draws the floawable's (graph's) for plotting the data
# Class for Graph 1: Line graph for Hours Used per Month for the year
class flowable_fig(reportlab.platypus.Flowable):
    def __init__(self, imgdata):
        reportlab.platypus.Flowable.__init__(self)
        self.img = reportlab.lib.utils.ImageReader(imgdata)       
    def draw(self):
        # Draw the Hour per Month graph, set the x&y position, set the height and width 
        self.canv.drawImage(self.img, 75, -10, height = -2.5*inch, width = 4.25*inch)
        
# Class for Graph 2: Bar chart for overall hours for each task for the year     
class flowable_fig2(reportlab.platypus.Flowable):
    def __init__(self, imgdata2):
        reportlab.platypus.Flowable.__init__(self)
        self.img2 = reportlab.lib.utils.ImageReader(imgdata2)       
    def draw(self):
        # Draw the Overall Hours for each Task graph, set the x&y position, set the height and width 
        self.canv.drawImage(self.img2, 20, -185, height = -2.5*inch, width = 5*inch)

# Class for Graph 3: Bar chart on Hours for each Business Unit for the year
class flowable_fig3(reportlab.platypus.Flowable): 
    def __init__(self, imgdata3):
        reportlab.platypus.Flowable.__init__(self)
        self.img3 = reportlab.lib.utils.ImageReader(imgdata3)       
    def draw(self):
        # Draw the Hours for each Business Unit graph, set the x&y position, set the height and width 
        self.canv.drawImage(self.img3, 0, -430, height = -2.5*inch, width = 5*inch)
        
# Class for Graph 4: Line graph for each Business Unit Hours Used 
class flowable_fig4(reportlab.platypus.Flowable):
    def __init__(self, imgdata4):
        reportlab.platypus.Flowable.__init__(self)
        self.img4 = reportlab.lib.utils.ImageReader(imgdata4)       
    def draw(self):
        # Draw the Hour per Month graph, set the x&y position, set the height and width 
        self.canv.drawImage(self.img4, 75, -10, height = -2.5*inch, width = 4*inch)
        
# Class for Graph 5: Line graph for comparing Quarters      
class flowable_fig5(reportlab.platypus.Flowable):
    def __init__(self, imgdata5):
        reportlab.platypus.Flowable.__init__(self)
        self.img5 = reportlab.lib.utils.ImageReader(imgdata5)       
    def draw(self):
        # Draw the Overall Hours for each Task graph, set the x&y position, set the height and width 
        self.canv.drawImage(self.img5, 75, -10, height = -2.5*inch, width = 4*inch)
        
# Class for Graph 6: Line Graph for quarterly report for MBUI
class flowable_fig6(reportlab.platypus.Flowable):
    def __init__(self, imgdata6):
        reportlab.platypus.Flowable.__init__(self)
        self.img6 = reportlab.lib.utils.ImageReader(imgdata6)        
    def draw(self):
        # Draw the Hour per Month graph, set the x&y position, set the height and width 
        self.canv.drawImage(self.img6, 75, -225, height = -2.5*inch, width = 4*inch)