import os
import pymysql.cursors

## Function to grab DB connection
def getConnection():
    # Create config with credentials 
    conf = {'host': os.environ['SQL_HOST'],
            'user': os.environ['SQL_USER'],
            'password': os.environ['SQL_PWD'],
            'db': os.environ['SQL_DB']}
    # Update the config 
    conf.update({'charset': 'utf8mb4',
                 'cursorclass': pymysql.cursors.SSDictCursor})
    # Create connection
    con = pymysql.connect(**conf)
    
    return con