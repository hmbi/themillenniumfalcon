# The Millennium Falcon

"The fastest hunk of junk in the galaxy!"

## Introduciton 

The Millennium Falcon is a script written in Python 3.6 that quickly geneterates YTD time sheet PDF reports for all of the VP's. The reports will show two years on it, the past year compared to the current 
year YTD. This script it meant to be run after every quarter is finsihed. 

## Overview 

TheMillenniumFalcon1.3.py - The script that generates the reports

figures.py - Script that holds flowables(graphs) classes

connection.py - Holds a method for storing the MySQL credentials

TheMillenniumFalcon1.1.ipynb - A Jupyter Notebook version of the script 

Timsheet Hour Troubleshooting.twb -  A Tableau workbook made for spot checking the reports

2018-TargetHours.xlsx - Excel file that holds the target hours for each client

VP's folder -  Contains the reports for each VP

pickle_objects folder - The data is stored in "pickles" so that it can be accessed in each function

## Running the script 

**Connection.py must be in this folder in order to correctly connect to the time sheets MySQL DB. The Credentials are stored in the local .bash_profile.**

1. Clone this report by opening the terminal window and typing ```git clone https://timbo1127@bitbucket.org/hmbi/themillenniumfalcon.git```. This will clone the repo to your local environment. 

2. In the terminal, navigate to where the repo was cloned. This is where the script can be run. The parameters need to be called via the command line. For example, if one wants to run the script to generate
   reports for 2018 Q1, the dates need to be called in the command line like so:
   
 ```python ./TheMillenniumFalcon1.1.py -py 2017 -cy 2018 -pd1 '2017-01--1' -pd2 '2017-12-31' -cd1 '2018-01-01' -cd2 '2018-03-31' -m 'June' -pm1 '2018-03-01' -pm2 '2018-03-31' -pdf_name ' 2018 Timesheets.pdf'```
 ```-pdf_title '2018 Timesheets' -target '2018-TargetHours.xlsx' -sheet '2018 Hourly Targets'```
 
	Where, 
	-py = past year
	-cy = current year
	-pd1, -pd2 = past year date range for query
	-cd1, -cd2 = current year date range for query
	-m = Last month in quater
	-pm1, -pm2 = previous month date range for query (the previous date range is the last month in each quater, so for Q1 it's March, for Q2 it's June)
	-pdf_name, -pdf_title = PDF file name and PDF page title 
	-target, -sheet = the excel file that holds the target hour data and the seet that the data is on in the excel file

### The Process 

There are a few things that make this script go:

1. Import the time sheet data via an SQL query and the target hours via an csv file. 

2. Join the two data sources together. 

3. Shape the data using pivot_table and groupby, subset the data into data frames that can be put into the PDF reports. 

4. Create class objects that hold the flowables (graphs). 

5. Generate the PDF reports using Reportlab. There are two different for loops that create the PDF's:

	a. The first for loop creates PDF reports for all clients who have a single business unit.
	
	b. The second for loop creates PDF reports for all clients who have multiple business units.


## Dependencies 

**For bringing in & manipulating data**

pandas 

numpy

pickle

**For command line parameters**
argparse

**For plotting**

matplotlib.pyplot

plt.style.use('ggplot')

**For generating the PDF reports**

cStringIO

from PIL import Image

from reportlab.pdfgen import canvas

from reportlab.lib.units import inch, cm

from reportlab.lib.utils import ImageReader

from reportlab.lib.pagesizes import letter

from reportlab.lib import colors

from reportlab.platypus import Table, TableStyle, Paragraph, SimpleDocTemplate, Image, PageBreak, Spacer

from reportlab.lib.styles import getSampleStyleSheet

from reportlab.lib.pagesizes import inch

import reportlab.lib, reportlab.platypus