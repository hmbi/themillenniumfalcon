'''
The Millennium Falcon 
Version 1.3 
A script that generate PDF reports for time sheets

example: 

python ./TheMillenniumFalcon1.2.py -py 2017 -cy 2018 -pd1 '2017-01-01' -pd2 '2017-12-31' 
-cd1 '2018-01-01' -cd2 '2018-09-30' -m 'September' -pm1 '2018-09-01' -pm2 '2018-09-30' -pdf_name ' 2018 Timesheets.pdf' 
-pdf_title ' 2018 Timesheets' -target '2018-TargetHours.xlsx' -sheet '2018 Hourly Targets'

'''
import os
import io
import connection
import argparse
import pickle
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import reportlab.lib, reportlab.platypus
from PIL import Image
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.utils import ImageReader
from reportlab.lib.pagesizes import letter
from reportlab.lib import colors
from reportlab.platypus import Table, TableStyle, Paragraph, SimpleDocTemplate, Image, PageBreak, Spacer
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import inch
from figures import *

def excel_data(args):
    '''Reads in excel file with target hours'''

    current_Target_Hours = pd.ExcelFile("{target}".format(target=args.target))
    current_Target_Hours = current_Target_Hours.parse("{sheet}".format(sheet=args.sheet))
    current_Target_Hours.rename(columns = {'2018 Target Hours':'hourTarget{current_year}'.format(current_year=args.current_year)}, inplace = True)
    pd.to_pickle(current_Target_Hours, "./pickle_objects/current_Target_Hours.pkl")

def query_data(args):
    '''
    Connects the to DB, performs 3 queries which are stored in a Pandas df. 
    Saves the Pandas df in Pickle object

    1st query: the past year
    2nd query: the current year to date
    3rd query: the last month in the current YTD quarter 

    '''
    con = connection.getConnection()
    print('Connection is a Roger!')
    current_Target_Hours = pd.read_pickle("./pickle_objects/current_Target_Hours.pkl")
    
    query = "SELECT c_name AS client, b_name AS unit, e_date AS date, t_name AS task, sum(e_duration / 60) AS hours FROM ts WHERE e_date BETWEEN '{}' and '{}' GROUP BY client, unit, date, task ORDER BY client, unit, date, task".format(args.past_date1, args.past_date2)
    past = pd.read_sql(query, con)        
    past['year'] = pd.DatetimeIndex(past['date']).year
    past['month'] = pd.DatetimeIndex(past['date']).month
    MonDict = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May', 6 : 'June', 7 : 'July', 8 : 'August',
                9 : 'September', 10 : 'October', 11 : 'November', 12 : 'December'}
    past = past.assign(month = past['month'].map(MonDict))
    past = past.rename(index=str, columns={'client':'Client'})
    past_full = pd.merge(past, current_Target_Hours, 
                            on = 'Client',
                            how = 'inner')
    past_full = past_full.rename(index=str, columns={'unit':'Unit', 'date':'Date', 'task':'Task', 'month':'Month',
                                                     'hourTarget2018':'Target Hours {current_year}'.format(current_year=args.current_year),
                                                     'hours':'Hours Used {past_year}'.format(past_year=args.past_year), 'year':'2017'})
    past_full['Month'] = pd.Categorical(past_full['Month'], 
                                            ordered=True,
                                            categories=['January','February','March','April','May','June','July',
                                                         'August','September','October','November','December'])
    past_full['Target Hours {current_year}'.format(current_year=args.current_year)] = past_full['Target Hours {current_year}'.format(current_year=args.current_year)].fillna(0).apply(np.int64)
    past_full = past_full.rename(index=str, columns={'hourTarget2018':'Target Hours {current_year}'.format(current_year=args.current_year)})
    
    SHARED_UNITS = past_full['Unit'].isin(['B2B', 'Brand', 'Branding', 'Corporate', 'OOH'])
    past_full.at[SHARED_UNITS, 'Unit'] = past_full['Client'] + ' ' + past_full['Unit']

    pd.to_pickle(past_full, "./pickle_objects/past_full.pkl")
    
    query2 = "SELECT c_name AS client, b_name AS unit, e_date AS date, t_name AS task, sum(e_duration / 60) AS hours FROM ts WHERE e_date BETWEEN '{}' and '{}' GROUP BY client, unit, date, task ORDER BY client, unit, date, task".format(args.current_date1, args.current_date2)
    current = pd.read_sql(query2, con)        
    current['year'] = pd.DatetimeIndex(current['date']).year
    current['month'] = pd.DatetimeIndex(current['date']).month
    MonDict = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May', 6 : 'June', 7 : 'July', 8 : 'August',
                9 : 'September', 10 : 'October', 11 : 'November', 12 : 'December'}
    current = current.assign(month = current['month'].map(MonDict))
    current = current.rename(index=str, columns={'client':'Client'})
    current_full = pd.merge(current, current_Target_Hours, 
                            on = 'Client',
                            how = 'inner')
    current_full = current_full.rename(index=str, columns={'unit':'Unit', 'date':'Date', 'task':'Task', 'month':'Month', 
                                                           'hours':'Hours Used {current_year}'.format(current_year=args.current_year),
                                                           'year':'2018', 'hourTarget2018':'Target Hours {current_year}'.format(current_year=args.current_year)})
    current_full['Month'] = pd.Categorical(current_full['Month'], 
                                             ordered=True,
                                             categories=['January','February','March','April','May','June','July',
                                                         'August','September','October','November','December'])
    current_full['Target Hours {current_year}'.format(current_year=args.current_year)] = current_full['Target Hours {current_year}'.format(current_year=args.current_year)].fillna(0).apply(np.int64)
    current_full = current_full.rename(index=str, columns={'hourTarget2017':'Target Hours {past_year}'.format(past_year=args.past_year)})
    current_full.fillna(current_full.mode().iloc[0])
    
    SHARED_UNITS = current_full['Unit'].isin(['B2B', 'Brand', 'Branding', 'Corporate', 'OOH'])
    current_full.at[SHARED_UNITS, 'Unit'] = current_full['Client'] + ' ' + current_full['Unit']

    pd.to_pickle(current_full, "./pickle_objects/current_full.pkl")

    query3 = "SELECT c_name AS client, b_name AS unit, e_date AS date, t_name AS task, sum(e_duration / 60) AS hours FROM ts WHERE e_date BETWEEN '{}' and '{}' GROUP BY client, unit, date, task ORDER BY client, unit, date, task".format(args.prev_month1, args.prev_month2)
    prev_month = pd.read_sql(query3, con)        
    prev_month['year'] = pd.DatetimeIndex(prev_month['date']).year
    prev_month['month'] = pd.DatetimeIndex(prev_month['date']).month
    MonDict = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May', 6 : 'June', 7 : 'July', 8 : 'August',
                9 : 'September', 10 : 'October', 11 : 'November', 12 : 'December'}
    prev_month = prev_month.assign(month = prev_month['month'].map(MonDict))
    prev_month = prev_month.rename(index=str, columns={'client':'Client', 'unit':'Unit', 'date':'Date', 'task':'Task', 'month':'Month', 
                                                        'hours':'{month} Hours Used {current_year}'.format(current_year=args.current_year, month=args.month),
                                                        'year':'Year'})
    prev_month['Month'] = pd.Categorical(prev_month['Month'], 
                                         ordered=True,
                                         categories=['January','February','March','April','May','June','July',
                                                     'August','September','October','November','December'])
    
    SHARED_UNITS = prev_month['Unit'].isin(['B2B', 'Brand', 'Branding', 'Corporate', 'OOH'])
    prev_month.at[SHARED_UNITS, 'Unit'] = prev_month['Client'] + ' ' + prev_month['Unit']

    pd.to_pickle(prev_month, "./pickle_objects/prev_month.pkl")

def shape_data(args):
    ''' Manipulates data into proper format for reports'''

    past_full = pd.read_pickle("./pickle_objects/past_full.pkl")
    current_full = pd.read_pickle("./pickle_objects/current_full.pkl")
    prev_month = pd.read_pickle("./pickle_objects/prev_month.pkl")

    client_unit_table_past = pd.pivot_table(past_full, values='Hours Used {past_year}'.format(past_year=args.past_year), index=['Client','Task'], aggfunc=sum)
    client_unit_table_past.fillna(0, inplace=True)
    for Client in client_unit_table_past:
        client_unit_table_past = pd.concat([client_unit_table_past, client_unit_table_past.sum(level=0).assign(Task='Year Total').set_index('Task', append=True)])
    
    client_unit_table_past = client_unit_table_past.reset_index().sort_values(['Client', 'Hours Used {past_year}'.format(past_year=args.past_year)], ascending=[True, True]).set_index(['Client', 'Task'])
    client_unit_table_current = pd.pivot_table(current_full, values='Hours Used {current_year}'.format(current_year=args.current_year), index=['Client','Task'], aggfunc=sum)
    client_unit_table_current.fillna(0, inplace=True)
    for Client in client_unit_table_current:
        client_unit_table_current = pd.concat([client_unit_table_current, client_unit_table_current.sum(level=0).assign(Task='Year Total').set_index('Task', append=True)])
    
    client_unit_table_current = client_unit_table_current.reset_index().sort_values(['Client', 'Hours Used {current_year}'.format(current_year=args.current_year)], ascending=[True, True]).set_index(['Client', 'Task'])
    client_unit_table = pd.concat([client_unit_table_past, client_unit_table_current], axis=1, join='inner')
    pd.to_pickle(client_unit_table, "./pickle_objects/client_unit_table.pkl")

    client_unit = pd.pivot_table(current_full, values='Hours Used {current_year}'.format(current_year=args.current_year), index=['Client','Unit', 'Task'], aggfunc=sum)
    client_unit.fillna(0, inplace=True)
    client_unit = client_unit.reset_index().sort_values(['Client', 'Unit', 'Hours Used {current_year}'.format(current_year=args.current_year)], ascending=[True, True, True]).set_index(['Client', 'Unit', 'Task'])
    pd.to_pickle(client_unit, "./pickle_objects/client_unit.pkl")

    MBUI_month_graph_current = current_full.groupby(['Unit', 'Month']).agg({'Hours Used {current_year}'.format(current_year=args.current_year): 'sum'})
    MBUI_month_graph_current = MBUI_month_graph_current.reset_index().sort_values(['Unit', 'Month', 'Hours Used {current_year}'.format(current_year=args.current_year)], ascending=[False, True, True]).set_index(['Unit', 'Month'])
    MBUI_month_graph_past = past_full.groupby(['Unit', 'Month']).agg({'Hours Used {past_year}'.format(past_year=args.past_year): 'sum'})
    MBUI_month_graph_past = MBUI_month_graph_past.reset_index().sort_values(['Unit', 'Month', 'Hours Used {past_year}'.format(past_year=args.past_year)], ascending=[False, True, True]).set_index(['Unit', 'Month'])
    MBUI_month_graph = pd.concat([MBUI_month_graph_past, MBUI_month_graph_current], axis=1).reindex(MBUI_month_graph_current.index)
    pd.to_pickle(MBUI_month_graph, "./pickle_objects/MBUI_month_graph.pkl")

    client_month_past = pd.pivot_table(past_full, values='Hours Used {past_year}'.format(past_year=args.past_year), index=['Client','Month'], aggfunc=sum)
    client_month_current = pd.pivot_table(current_full, values='Hours Used {current_year}'.format(current_year=args.current_year), index=['Client','Month'], aggfunc=sum)
    client_month_graph = pd.concat([client_month_past, client_month_current], axis=1).reindex(client_month_current.index)
    pd.to_pickle(client_month_graph, "./pickle_objects/client_month_graph.pkl")

    MBUI_unit_hours_table = current_full.groupby(['Client', 'Unit']).agg({'Hours Used {current_year}'.format(current_year=args.current_year): 'sum'})
    for Client in MBUI_unit_hours_table:
        MBUI_unit_hours_table = pd.concat([MBUI_unit_hours_table, MBUI_unit_hours_table.sum(level=0).assign(Unit='Total').set_index('Unit', append=True)])
    
    MBUI_unit_hours_table = MBUI_unit_hours_table.reset_index().sort_values(['Client', 'Hours Used {current_year}'.format(current_year=args.current_year)], ascending=[False, True]).set_index(['Client', 'Unit'])
    pd.to_pickle(MBUI_unit_hours_table, "./pickle_objects/MBUI_unit_hours_table.pkl")

    MBUI_unit_task_hours = current_full.groupby(['Client', 'Unit', 'Task']).agg({'Hours Used {current_year}'.format(current_year=args.current_year): 'sum'})
    pd.to_pickle(MBUI_unit_task_hours, "./pickle_objects/MBUI_unit_task_hours.pkl")

    MBUI_unit_task_hours_table_current = current_full.groupby(['Unit', 'Task']).agg({'Hours Used {current_year}'.format(current_year=args.current_year): 'sum'})
    for Unit in MBUI_unit_task_hours_table_current:
        MBUI_unit_task_hours_table_current = pd.concat([MBUI_unit_task_hours_table_current, MBUI_unit_task_hours_table_current.sum(level=0).assign(Task='Year Total').set_index('Task', append=True)])
    
    MBUI_unit_task_hours_table_current = MBUI_unit_task_hours_table_current.reset_index().sort_values(['Unit', 'Hours Used {current_year}'.format(current_year=args.current_year)], ascending=[False, True]).set_index(['Unit', 'Task'])
    MBUI_unit_task_hours_table_past = past_full.groupby(['Unit', 'Task']).agg({'Hours Used {past_year}'.format(past_year=args.past_year): 'sum'})
    for Unit in MBUI_unit_task_hours_table_past:
        MBUI_unit_task_hours_table_past = pd.concat([MBUI_unit_task_hours_table_past, MBUI_unit_task_hours_table_past.sum(level=0).assign(Task='Year Total').set_index('Task', append=True)])
    
    MBUI_unit_task_hours_table_past = MBUI_unit_task_hours_table_past.reset_index().sort_values(['Unit', 'Hours Used {past_year}'.format(past_year=args.past_year)], ascending=[False, True]).set_index(['Unit', 'Task'])
    MBUI_unit_task_hours_table = pd.concat([MBUI_unit_task_hours_table_past, MBUI_unit_task_hours_table_current], axis=1, join='inner')
    pd.to_pickle(MBUI_unit_task_hours_table, "./pickle_objects/MBUI_unit_task_hours_table.pkl")

    HoursUsed = pd.pivot_table(current_full, values='Hours Used {current_year}'.format(current_year=args.current_year), index=['Client'], aggfunc=sum)
    HoursUsed.fillna(0, inplace=True)
    TargetHours = pd.pivot_table(current_full, values='Target Hours {current_year}'.format(current_year=args.current_year), index=['Client'])
    TargetHours.fillna(0, inplace=True)
    TargetHoursCurrent = pd.concat([TargetHours, HoursUsed], axis=1, join='inner')
    TargetHoursCurrent['Remaining'] = TargetHoursCurrent['Target Hours {current_year}'.format(current_year=args.current_year)] - TargetHoursCurrent['Hours Used {current_year}'.format(current_year=args.current_year)].values
    pd.to_pickle(TargetHoursCurrent, "./pickle_objects/TargetHoursCurrent.pkl")

    previous_month_task = pd.pivot_table(prev_month, values='{month} Hours Used {current_year}'.format(current_year=args.current_year, month=args.month), index=['Unit', 'Task'], aggfunc=sum)
    previous_month_task.fillna(0, inplace=True)
    for Unit in previous_month_task:
        previous_month_task = pd.concat([previous_month_task, previous_month_task.sum(level=0).assign(Task='Month Total').set_index('Task', append=True)])
    
    previous_month_task = previous_month_task.reset_index().sort_values(['Unit', '{month} Hours Used {current_year}'.format(current_year=args.current_year, month=args.month)],
                                                                                                      ascending=[False, True]).set_index(['Unit', 'Task'])
    pd.to_pickle(previous_month_task, "./pickle_objects/previous_month_task.pkl")

    MBUI = current_full.groupby(['Client', 'Unit']).agg({'Hours Used {current_year}'.format(current_year=args.current_year): 'sum'})
    MBUI = MBUI.reset_index().sort_values(['Client', 'Hours Used {current_year}'.format(current_year=args.current_year)], ascending=[True, True]).set_index(['Client', 'Unit'])
    pd.to_pickle(MBUI, "./pickle_objects/MBUI.pkl")

def single_business(args):
    '''Loops through each client with single business units and creates a PDF report'''

    current_full = pd.read_pickle("./pickle_objects/current_full.pkl")
    TargetHoursCurrent = pd.read_pickle("./pickle_objects/TargetHoursCurrent.pkl")
    client_unit_table = pd.read_pickle("./pickle_objects/client_unit_table.pkl")
    client_month_graph = pd.read_pickle("./pickle_objects/client_month_graph.pkl")
    single = current_full.loc[current_full['NumUnits'] == 1]
    # Get the unique values for each client in SBUI
    clients = single['Client'].unique()
    # Loop through the clients in the SBUI data frame, create the table and plot each graph 
    for client in clients:        
        # Create an empty list to store data in (Called the Story)
        Story = []
        # Set up the style of the PDF
        styles = getSampleStyleSheet()
        # Name the PDF based on each client & make a title for each client's page
        doc = SimpleDocTemplate(client + "{pdf_name}".format(pdf_name=args.pdf_name), pagesize=letter)
        Story.append(Paragraph(client + "{pdf_title}".format(pdf_title=args.pdf_title), styles['Title']))       
        # Table 1: Define the data for target hours vs. hours used
        data = TargetHoursCurrent
        # SBUI_TargetHours_HoursUsed
        # Reset the index so row labels show up in the reportlab table
        df = data.reset_index()
        # Index each client
        df = df[df['Client'] == client]
        # Number of table header rows to repeat
        n = df.columns.nlevels 
        # If n is greater than 1, map the column values, else set the columns to a string and convert to a list
        if n > 1:
            labels = map(list, zip(*df.columns.values))
        else:
            labels = [df.columns[:,].values.astype(str).tolist()]
        # Set the values to a list 
        values = df.values.tolist()
        # Add the labels and values to "datalist"
        datalist = labels + values
        # Convert the datalist to a ReportLab Table
        table = Table(datalist)
        # Create the style of the table
        table.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black)]))       
        data_len = len(df)
        for each in range(data_len):
            if each % 2 == 0:
                bg_color = colors.whitesmoke
            else:
                bg_color = colors.lightgrey
            table.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color),
                                       ('BACKGROUND', (0,0), (3,0), colors.forestgreen),
                                       ('TEXTCOLOR', (0,0), (3,0), colors.white)]))
        # Append the table to the Story (PDF)
        Story.append(table)
        # Add a spacer after table 
        Story.append(Spacer(1, 0.50*inch))     
        # Table 2: Define the data for the total Hours per Task 
        data2 = client_unit_table
        # Reset the index so row labels show up in the reportlab table
        df2 = data2.reset_index()
        # Index each client
        df2 = df2[df2['Client'] == client]
        # Number of table header rows to repeat
        n2 = df2.columns.nlevels 
        # If n is greater than 1, map the column values, else set the columns to a string and convert to a list
        if n2 > 1:
            labels2 = map(list, zip(*df2.columns.values))
        else:
            labels2 = [df2.columns[:,].values.astype(str).tolist()]
        # Set the values to a list 
        values2 = df2.values.tolist()
        # Add the labels and values to "datalist"
        datalist2 = labels2 + values2
        # Convert the datalist to a ReportLab Table
        table2 = Table(datalist2)
        # Create the style of the table
        table2.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                                    ('ALIGN', (0,0), (0,0), 'CENTER')]))
        data_len = len(df2)
        for each in range(data_len):
            if each % 2 == 0:
                bg_color = colors.whitesmoke
            else:
                bg_color = colors.lightgrey
            table2.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color),
                                        ('BACKGROUND', (0,0), (3,0), colors.orange)]))
        # Append the table to the Story (PDF)
        Story.append(table2)            
        # Line Graph: Hours spent working for each client
        # Split the data based on the clients and hours
        split = client_month_graph.xs(client)       
        # Create the figure, x/y-axis, title, x-axis ticks
        fig = plt.figure()
        ax = split.plot (kind="line", color=('green', 'blue'))
        ax.set_ylabel('Hours Used')
        ax.set_xlabel('Month')
        ax.tick_params(axis='x', colors='blue')
        ax.tick_params(axis='y', colors='green')
        plt.title('Hours per Month: {past_year} Vs. {current_year}'.format(past_year=args.past_year, current_year=args.current_year))
        plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11], ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
        plt.tight_layout() # Fixes the margins 
        # Save the figures as a png's
        imgdata = io.BytesIO()
        plt.savefig(imgdata, format='png')
        # rewind the data
        imgdata.seek(0)
        # Save the plot in a flowable & append to the Story
        pic = flowable_fig(imgdata)
        Story.append(pic)        
        # Close the plots
        plt.close()        
        # Build the Story
        doc.build(Story)
        print(client + " complete!")
        print("___________________")
        print("                   ")

def multiple_business(args):
    '''Loops through each client with single business units and creates a PDF report'''

    current_full = pd.read_pickle("./pickle_objects/current_full.pkl")
    TargetHoursCurrent = pd.read_pickle("./pickle_objects/TargetHoursCurrent.pkl")
    client_unit_table = pd.read_pickle("./pickle_objects/client_unit_table.pkl")
    client_month_graph = pd.read_pickle("./pickle_objects/client_month_graph.pkl")
    client_unit = pd.read_pickle("./pickle_objects/client_unit.pkl")
    MBUI_unit_task_hours_table = pd.read_pickle("./pickle_objects/MBUI_unit_task_hours_table.pkl")
    previous_month_task = pd.read_pickle("./pickle_objects/previous_month_task.pkl")
    MBUI_month_graph = pd.read_pickle("./pickle_objects/MBUI_month_graph.pkl")
    MBUI_month_graph_current = pd.read_pickle("./pickle_objects/MBUI_month_graph.pkl")
    MBUI = pd.read_pickle("./pickle_objects/MBUI.pkl")
    multiple = current_full.loc[current_full['NumUnits'] == 0]
    # Get the unique values for each client in MBUI
    clients = multiple['Client'].unique()
    # Loop through the clients in the MBUI dataframe, create the table and plot each graph 
    for client in clients:       
        # Create an empty list to store data in (Called the Story)
        Story = []
        # Set up the style of the PDF
        styles = getSampleStyleSheet()
        # Name the PDF based on each client & make a title for each client's page
        doc = SimpleDocTemplate(client + "{pdf_name}".format(pdf_name=args.pdf_name), pagesize=letter)
        Story.append(Paragraph(client + "{pdf_title}".format(pdf_title=args.pdf_title), styles['Title']))        
        # Table 1: Define the data for target hours vs. hours used for 
        data = TargetHoursCurrent
        # Reset the index so row labels show up in the reportlab table
        df = data.reset_index()
        # Index each client
        df = df[df['Client'] == client]
        # Number of table header rows to repeat
        n = df.columns.nlevels 
        # If n is greater than 1, map the column values, else set the columns to a string and convert to a list
        if n > 1:
            labels = map(list, zip(*df.columns.values))
        else:
            labels = [df.columns[:,].values.astype(str).tolist()]
        # Set the values to a list 
        values = df.values.tolist()
        # Add the labels and values to "datalist"
        datalist = labels + values
        # Convert the datalist to a ReportLab Table
        table = Table(datalist)
        # Create the style of the table
        table.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black)]))       
        data_len = len(df)
        for each in range(data_len):
            if each % 2 == 0:
                bg_color = colors.whitesmoke
            else:
                bg_color = colors.lightgrey
            table.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color),
                                       ('BACKGROUND', (0,0), (3,0), colors.forestgreen),
                                       ('TEXTCOLOR', (0,0), (3,0), colors.white)]))
        # Append the table to the Story (PDF)
        Story.append(table)        
        # Add a spacer after table 
        Story.append(Spacer(1, 0.50*inch))            
        # Table 2: Define the data for the total Hours per Task 
        data2 = client_unit_table
        # Reset the index so row labels show up in the reportlab table
        df2 = data2.reset_index()
        # Index each client
        df2 = df2[df2['Client'] == client]
        # Number of table header rows to repeat
        n2 = df2.columns.nlevels 
        # If n is greater than 1, map the column values, else set the columns to a string and convert to a list
        if n2 > 1:
            labels2 = map(list, zip(*df2.columns.values))
        else:
            labels2 = [df2.columns[:,].values.astype(str).tolist()]
        # Set the values to a list 
        values2 = df2.values.tolist()
        # Add the labels and values to "datalist"
        datalist2 = labels2 + values2
        # Convert the datalist to a ReportLab Table
        table2 = Table(datalist2)
        # Create the style of the table
        table2.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                                    ('ALIGN', (0,0), (0,0), 'CENTER')]))
        data_len = len(df2)
        for each in range(data_len):
            if each % 2 == 0:
                bg_color = colors.whitesmoke
            else:
                bg_color = colors.lightgrey
            table2.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color),
                                        ('BACKGROUND', (0,0), (3,0), colors.orange)]))
        # Append the table to the Story (PDF)
        Story.append(table2)           
        # Line Graph: Hours spent working for each client
        # Split the data based on the clients and hours
        split = client_month_graph.xs(client)       
        # Create the figure, x/y-axis, title, x-axis ticks
        fig = plt.figure()
        ax = split.plot (kind="line", color=('green', 'blue'))
        ax.set_ylabel('Hours Used')
        ax.set_xlabel('Month')
        ax.tick_params(axis='x', colors='blue')
        ax.tick_params(axis='y', colors='green')
        plt.title('Hours per Month: {past_year} Vs. {current_year}'.format(past_year=args.past_year, current_year=args.current_year))
        plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11], ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
        plt.tight_layout() # Fixes the margins 
        # Save the figures as a png's
        imgdata = io.BytesIO()
        plt.savefig(imgdata, format='png')
        # rewind the data
        imgdata.seek(0)
        # Save the plot in a flowable & append to the Story
        pic = flowable_fig(imgdata)
        Story.append(pic)       
        # Bar Graph 2: Overall hours spent on each unit for all clients 
        # Split the data based on the clients and hours
        split2 = MBUI.xs(client)
        # Create the figure, x/y-axis, title, x-axis ticks
        fig2 = plt.figure()
        ax2 = split2.plot(kind='barh', color='green')
        ax2.set_ylabel('Business Unit')
        ax2.set_xlabel('Hours Used')
        # plt.title('Business Unit Hours: {current_year}'.format(current_year=args.current_year))
        ax2 = plt.gca()
        ax2.tick_params(axis='x', colors='blue')
        ax2.tick_params(axis='y', colors='green')
        plt.tight_layout()       
        # Save the figures as a png's
        imgdata2 = io.BytesIO()
        plt.savefig(imgdata2, format='png')
        # rewind the data
        imgdata2.seek(0)
        #  Save the plot in a flowable & append to the Story
        pic2 = flowable_fig2(imgdata2)
        Story.append(pic2)        
        # Page break
        Story.append(PageBreak())        
        ## Create a loop that iterates through each Client's BUI Task's and prints them to a new page in the PDF        
        # Reset the index for the table BUI vs. Hours Used
        newdf = client_unit.reset_index()
        units = newdf[newdf['Client'] == client]["Unit"].unique()       
        for unit in units:
            # Add the title of each Business Unit pages
            Story.append(Paragraph(unit + " Details", styles['Title']))
            # Table 3: Create table for task's comparing past and current year 
            data3 = MBUI_unit_task_hours_table
            # Reset the index so row labels show up in the reportlab table
            df3 = data3.reset_index()
            # Index each client
            df3 = df3[df3['Unit'] == unit]
            # Number of table header rows to repeat
            n3 = df3.columns.nlevels 
            # If n is greater than 1, map the column values, else set the columns to a string and convert to a list
            if n3 > 1:
                labels3 = map(list, zip(*df3.columns.values))
            else:
                labels3 = [df3.columns[:,].values.astype(str).tolist()]
            # Set the values to a list 
            values3 = df3.values.tolist()
            # Add the labels and values to "datalist"
            datalist3 = labels3 + values3
            # Convert the datalist to a ReportLab Table
            table3 = Table(datalist3)
            # Create the style of the table
            table3.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black)]))
            # Get length of dataframe
            data_len = len(df3)
            # For each row in the dataframe, color the odds whitesmoke and evens lightgrey
            for each in range(data_len):
                if each % 2 == 0:
                    bg_color = colors.whitesmoke
                else:
                    bg_color = colors.lightgrey
                        
                table3.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color),
                                            ('BACKGROUND', (0,0), (3,0), colors.forestgreen),
                                            ('TEXTCOLOR', (0,0), (3,0), colors.white)]))               
            # Append the table to the Story (PDF)
            Story.append(table3)           
            # Add a spacer after table 
            Story.append(Spacer(1, 0.50*inch))
            # Table 4: Create table for previous month task's 
            data4 = previous_month_task
            # Reset the index so row labels show up in the reportlab table
            df4 = data4.reset_index()
            # Index each client
            df4 = df4[df4['Unit'] == unit]
            # Number of table header rows to repeat
            n4 = df4.columns.nlevels 
            # If n is greater than 1, map the column values, else set the columns to a string and convert to a list
            if n4 > 1:
                labels4 = map(list, zip(*df4.columns.values))
            else:
                labels4 = [df4.columns[:,].values.astype(str).tolist()]
            # Set the values to a list 
            values4 = df4.values.tolist()
            # Add the labels and values to "datalist"
            datalist4 = labels4 + values4
            # Convert the datalist to a ReportLab Table
            table4 = Table(datalist4)
            # Create the style of the table
            table4.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.25, colors.black)]))
            # Get length of dataframe                            
            data_len = len(df4)
            # For each row in the dataframe, color the odds whitesmoke and evens lightgrey
            for each in range(data_len):
                if each % 2 == 0:
                    bg_color = colors.whitesmoke
                else:
                    bg_color = colors.lightgrey                        
                table4.setStyle(TableStyle([('BACKGROUND', (0, each), (-1, each), bg_color),
                                            ('BACKGROUND', (0,0), (3,0), colors.orange)]))               
            # Append the table to the Story (PDF)
            Story.append(table4)           
            # Graph 4: Hours spent working for each BUI for the year
            # Split the data based on the clients and hours
            try:
                split4 = MBUI_month_graph.xs(unit)
                # Create the figure, x/y-axis, title, x-axis ticks
                fig4 = plt.figure()
                ax4 = split4.plot(kind='line', color=('green', 'blue'))
                ax4.set_ylabel('Hours Used')
                ax4.set_xlabel('Month')
                ax4.tick_params(axis='x', colors='blue')
                ax4.tick_params(axis='y', colors='green')
                plt.title('Business Unit Hours per Month: {past_year} Vs. {current_year}'.format(past_year=args.past_year, current_year=args.current_year))
                plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11], ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
                plt.tight_layout() # Fixes the margins 
                # Save the figures as a png's
                imgdata4 = io.BytesIO()
                plt.savefig(imgdata4, format='png')
                # rewind the data
                imgdata4.seek(0)
                # Save the plot in a flowable & append to the Story
                pic4 = flowable_fig4(imgdata4)
                Story.append(pic4)
            
            except KeyError as e:
                split4 = MBUI_month_graph_current.xs(unit)
                # Create the figure, x/y-axis, title, x-axis ticks
                fig4 = plt.figure()
                ax4 = split4.plot(kind='line', color='green')
                ax4.set_ylabel('Hours Used')
                ax4.set_xlabel('Month')
                ax4.tick_params(axis='x', colors='blue')
                ax4.tick_params(axis='y', colors='green')
                plt.title('Business Unit Hours per Month: {past_year} Vs. {current_year}'.format(past_year=args.past_year, current_year=args.current_year))
                plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11], ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
                plt.tight_layout() # Fixes the margins 
                # Save the figures as a png's
                imgdata4 = io.BytesIO()
                plt.savefig(imgdata4, format='png')
                # rewind the data
                imgdata4.seek(0)
                # Save the plot in a flowable & append to the Story
                pic4 = flowable_fig4(imgdata4)
                Story.append(pic4)
            # Page break
            Story.append(PageBreak())
        print(client + " complete!")
        print("___________________")
        print("                   ")
        # Close the plots
        plt.close()
        # Build the Story
        doc.build(Story)
    print("                      ")    
    print("Reports are complete!!")
    print("                      ")

if __name__ == '__main__':
# Create the parser arguments 
    parser = argparse.ArgumentParser(description = "Pick your poison!!")
    # Add arguments
    parser.add_argument('-py', type = int, action = 'store', dest = 'past_year', help = 'The past year')
    parser.add_argument('-cy', type = int, action = 'store', dest = 'current_year', help = 'The current year')
    parser.add_argument('-pd1', type = str, action = 'store', dest = 'past_date1', help = 'January 01 of the past year(must be inside quotes), yyyy-mm-dd')
    parser.add_argument('-pd2', type = str, action = 'store', dest = 'past_date2', help = 'December 31 of past year(must be inside quotes), yyyy-mm-dd')
    parser.add_argument('-cd1', type = str, action = 'store', dest = 'current_date1', help = 'Start date of the current year(must be inside quotes), yyyy-mm-dd')
    parser.add_argument('-cd2', type = str, action = 'store', dest = 'current_date2', help = 'End date of the quarter(must be inside quotes), yyyy-mm-dd, ex. reporting on Q1 2018, the last day of March, 2018-03-31')
    parser.add_argument('-m', type = str, action = 'store', dest = 'month', help = 'The month in the quarter you want to run report for(must be a input as a month starting with a capital letter, ex. reporting on Q1 2018, the last month is March')
    parser.add_argument('-pm1', type = str, action = 'store', dest = 'prev_month1', help = 'Start date of last month in the quarter(must be inside quotes), yyyy-mm-dd, ex. reporting on Q1 2018, 2018-03-01 is prev_month1')
    parser.add_argument('-pm2', type = str, action = 'store', dest = 'prev_month2', help = 'End date of last month in the quarter(must be inside quotes), yyyy-mm-dd, ex. reporting on Q1 2018, 2018-03-31 is prev_month2')
    parser.add_argument('-pdf_name', type = str, action = 'store', dest = 'pdf_name', help = 'Name of the file (starts w/ a space), ex. " 2018 Timesheets.pdf"')
    parser.add_argument('-pdf_title', type = str, action = 'store', dest = 'pdf_title', help = 'Title of the report (starts w/ a space), ex. " 2018 Timesheets"')
    parser.add_argument('-target', type = str, action = 'store', dest = 'target', help = 'Name of the excel file, ex. "2018-TargetHours.xlsx"')
    parser.add_argument('-sheet', type = str, action = 'store', dest = 'sheet', help = 'Name of the sheet in excel file, ex. "2018 Target Hours"')
    args = parser.parse_args()
    # Grab data
    excel_data(args=args)
    query_data(args=args)
    shape_data(args=args)
    # Generate single business reports 
    single_business(args=args)
    # Generate multiple business reports 
    multiple_business(args=args)